# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU NXP LPC1759FBD80.

# Projekt PCB

Schemat: [doc/LPC1759_EVB_V1_0_SCH.pdf](doc/LPC1759_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/LPC1759_EVB_V1_0_3D.pdf](doc/LPC1759_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
